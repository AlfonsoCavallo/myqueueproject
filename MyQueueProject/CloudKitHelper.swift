//
//  CloudKitHelper.swift
//  MyQueueProject
//
//  Created by Angelo Vistocco on 16/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit
import CloudKit

class CloudKitHelper: NSObject {
    struct RecordTyper {
        static let queue = "ciao"
    }
    
//    MARK: - errors
    enum ClouKitHelperError: Error {
        case recordFailure
        case recordIDFailure
        case castFailure
        case cursorFailure
    }
    
//    MARK: - saving to Cloudkit
    static func save(queue: MyQueue, completion: @escaping (Result<MyQueue, Error>) -> ()) {
        let queueRecord = CKRecord(recordType: RecordTyper.queue)
        var uuidList = [String]()
        
        for person in queue.people.array {
            uuidList.append(person.uuid)
        }
        
        queueRecord["title"] = queue.title as CKRecordValue
        queueRecord["description"] = queue.desc as CKRecordValue?
        queueRecord["number"] = queue.people.count as CKRecordValue
        queueRecord["waitingTime"] = queue.avgWaitTime as CKRecordValue
        queueRecord["peopleList"] = uuidList as CKRecordValue
        queueRecord["beaconUuid"] = queue.beaconUUID as CKRecordValue
        
        CKContainer.default().publicCloudDatabase.save(queueRecord) { (record, err) in
            DispatchQueue.main.async {
                            if let err = err {
                        completion(.failure(err))
                        return
                    }
                    guard let record = record else {
                        completion(.failure(ClouKitHelperError.recordFailure))
                        return
                    }
                    let id = record.recordID
                    guard let title = record["title"] as? String else {
                        completion(.failure(ClouKitHelperError.castFailure))
                        return
                    }
                    guard let description = record["description"] as? String else {
                        completion(.failure(ClouKitHelperError.castFailure))
                        return
                    }
                    guard let number = record["number"] as? Int else {
                        completion(.failure(ClouKitHelperError.castFailure))
                        return
                    }
                    guard let waitingTime = record["waitingTime"] as? Double else {
                        completion(.failure(ClouKitHelperError.castFailure))
                        return
                    }
                    guard let peopleList = record["peopleList"] as? [String] else {
                        completion(.failure(ClouKitHelperError.castFailure))
                        return
                    }
                    guard let beaconUuid = record["beaconUuid"] as? String else {
                        completion(.failure(ClouKitHelperError.castFailure))
                        return
                    }
                    let element = MyQueue(people: queue.people, title: queue.title, avgWaitTime: queue.avgWaitTime, beaconUUID: queue.beaconUUID, desc: queue.desc ?? "", recordID: id)
                    completion(.success(element))
                }
            }
    }
    
    
//    MARK: -fetching from CloudKit
    static func fetch(completion: @escaping (Result<[MyQueue], Error>) -> ()) {
        var list = [MyQueue]()
        
        let pred = NSPredicate(value: true)
        let sort = NSSortDescriptor(key: "creationDate", ascending: false)
        let query = CKQuery(recordType: RecordTyper.queue, predicate: pred)
        query.sortDescriptors = [sort]
        
        let operation = CKQueryOperation(query: query)
        operation.desiredKeys = ["title", "description", "number", "peopleList", "waitingTime", "beaconUuid"]
        operation.resultsLimit = 50
        
        operation.recordFetchedBlock = { record in
            DispatchQueue.main.async {
                let id = record.recordID
                guard let title = record["title"] as? String else {
                    completion(.failure(ClouKitHelperError.castFailure))
                    return
                }
                guard let number = record["number"] as? Int else {
                    completion(.failure(ClouKitHelperError.castFailure))
                    return
                }
                guard let description = record["description"] as? String else {
                    completion(.failure(ClouKitHelperError.castFailure))
                    return
                }
                guard let peopleList: [String] = record["peopleList"] as? [String] else {
                    completion(.failure(ClouKitHelperError.castFailure))
                    return
                }
                guard let waitingTime = record["waitingTime"] as? Double else {
                    completion(.failure(ClouKitHelperError.castFailure))
                    return
                }
                guard let beaconUuid = record["beaconUuid"] as? String else {
                    completion(.failure(ClouKitHelperError.castFailure))
                    return
                }
                var newQueue = Queue<User>()
                for person in peopleList {
                    newQueue.enqueue(User(uuid: person))
                }
                let element = MyQueue(people: newQueue, title: title, avgWaitTime: Float(waitingTime), beaconUUID: beaconUuid, desc: description, recordID: id)
                list.append(element)
            }
        }
        
        operation.queryCompletionBlock = { (_, err) in
            DispatchQueue.main.async {
                    if let err = err {
                    completion(.failure(err))
                    return
                    } else {
                        completion(.success(list))
                }
            }
        }
        
        CKContainer.default().publicCloudDatabase.add(operation)
           
    }
    
//    MARK: - delete from CloudKit
    static func delete(recordID: CKRecord.ID, completion: @escaping (Result<CKRecord.ID, Error>) -> ()){
        CKContainer.default().publicCloudDatabase.delete(withRecordID: recordID) { (recordID, err) in
            DispatchQueue.main.async {
                            if let err = err {
                    completion(.failure(err))
                    return
                }
                guard let recordID = recordID else {
                    completion(.failure(ClouKitHelperError.castFailure))
                    return
                }
                completion(.success(recordID))
            }
        }
    }
    
//    MARK: - modify in CloudKit
    static func modify(queue: MyQueue, completion: @escaping (Result<MyQueue, Error>) -> ()) {
        guard let recordID = queue.recordID else { return }
        CKContainer.default().publicCloudDatabase.fetch(withRecordID: recordID) { (record, err) in
            DispatchQueue.main.async {
                if let err = err {
                    completion(.failure(err))
                    return
                }
                guard let record = record else { return }
                record["number"] = queue.people.count as CKRecordValue
                var newPeopleList = [String]()
                for person in queue.people.array {
                    newPeopleList.append(person.uuid)
                }
                record["peopleList"] = newPeopleList
                
                CKContainer.default().publicCloudDatabase.save(record) { (record, err) in
                    DispatchQueue.main.async {
                        if let err = err {
                            completion(.failure(err))
                            return
                        }
                        guard let record = record else { return }
                        let id = record.recordID
                        guard let number = record["number"] as? Int else { return }
                        guard let newPeopleList = record["peopleList"] as? [String] else { return }
                        var peopleArray = Queue<User>()
                        for person in newPeopleList {
                            peopleArray.array.append(User(uuid: person))
                        }
                        let element = MyQueue(people: peopleArray, title: queue.title, avgWaitTime: queue.avgWaitTime, beaconUUID: queue.beaconUUID, desc: queue.desc ?? "", recordID: id)
                        completion(.success(element))
                    }
                }
            }
        }
    }
}
