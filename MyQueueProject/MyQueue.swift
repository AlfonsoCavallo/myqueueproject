//
//  MyQueue.swift
//  MyQueueProject
//
//  Created by Tommaso Di Maio on 13/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import Foundation
class MyQueue{
    var people:Queue<User>
    var title:String
    var desc:String?
    var avgWaitTime:Float
    var helpText:String
    var beaconUUID:String
    init(people:Queue<User>,title:String,avgWaitTime:Float,helpText:String,beaconUUID:String) {
        self.people=people
        self.title=title
        self.avgWaitTime=avgWaitTime
        self.helpText=helpText
        self.beaconUUID=beaconUUID
        self.desc=nil
    }
    init(people:Queue<User>,title:String,avgWaitTime:Float,helpText:String,beaconUUID:String,
         desc: String) {
        self.people=people
        self.title=title
        self.avgWaitTime=avgWaitTime
        self.helpText=helpText
        self.beaconUUID=beaconUUID
        self.desc=desc
    }
}
