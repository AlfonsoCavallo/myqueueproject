//
//  BeaconUtilities.swift
//  MyQueueProject
//
//  Created by Angelo Vistocco on 15/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

class BeaconUtilities: NSObject, CLLocationManagerDelegate {
    
    // BEACON
    let beaconRegion: CLBeaconRegion //Il tipo di ricerca del Beacon
    var beaconSet: Set<String> //Il set dei beacon in zona
    var locationManager: CLLocationManager
    
    let user = User()
    
    
    // UTILITY
    var queueList: [MyQueue] //La lista delle code sul server
    var lock = DispatchSemaphore(value: 1);
    var currentQueueUUID: String //L'ID dell'attuale coda collegata al tasto
//    coda vuota, utile nelle funzioni cloud
    var emptyQueue = MyQueue(people: Queue<User>(), title: "", avgWaitTime: 0.0, helpText: "", beaconUUID: "", desc: "")
    var currentQueue : MyQueue? //L'attuale coda collegata al tasto
    
    // UTILITY
       
    // FUNZIONI DEI BEACON
    //Comincia a cercare i Beacons
    func startSearch(for region: CLBeaconRegion)
    {
        locationManager.startMonitoring(for: region); //Monitora il Beacon
        locationManager.startRangingBeacons(satisfying: region.beaconIdentityConstraint);//Calcola le distanze sui Beacon
    }
    
    //Interrompe la ricerca dei Beacons
    func stopSearch(for region: CLBeaconRegion)
    {
    locationManager.stopMonitoring(for: beaconRegion); //Interrompe il monitoraggio il Beacon
    locationManager.stopRangingBeacons(satisfying: beaconRegion.beaconIdentityConstraint); //Interrompe il calcolo delle distanze sui Beacon
    }
       
    // FUNZIONI DI UTILITA'
    //Imposta la prima coda joinabile come coda corrente
    func setupCurrentQueue()
    {
        //Filtra solo gli elementi joinabili
        let queueFiltered = queueList.filter()
        {
            $0.joinable;
        }
        //Se ci sono queue joinabili si aggancia alla prima
        if(queueFiltered.count > 0)
        {
            currentQueueUUID = queueFiltered[0].beaconUUID; //Seleziona il primo Beacon che ha trovato
            currentQueue = queueFiltered[0];
            //Il primo beacon trovato è quello più vicino?
        }
        else
        {
            currentQueueUUID = ""; //Il valore che gli assegna se non riesce a trovare un UUID valido
            currentQueue = nil;
        }
    }
       
    //Imposta il parametro come coda corrente
    func setupCurrentQueue(_ elem : MyQueue)
    {
        currentQueueUUID = elem.beaconUUID;
        currentQueue = elem;
    }
       
    //Aggiorna il database
    func listRefresh()
    {
        //CodeFor: Istruzione che aggiorna queueList da server
        lock.wait() //Prende il mutex
        CloudKitHelper.fetch { (result) in
            switch result {
            case .success(let newList):
//                self.queueList.removeAll()
                var joinedQueue: MyQueue?
                for queue in self.queueList {
                    if queue.joined {
                        joinedQueue = queue
                    }
                }
                self.queueList = newList
                for queue in self.queueList {
                    if queue.title == joinedQueue?.title{
                        queue.joined = true
                    }
                }

            case .failure(let err):
                print(err.localizedDescription)
            }
        }
//
        //Itera per trovare tutte le liste joinabili
        for elem in queueList
        {
            if(beaconSet.contains(elem.beaconUUID))
            {
                elem.joinable = true;
            }
            else
            {
                elem.joinable = false;
            }
        }
        //Se non ci sono code collegate al tasto ne collega una
        if(currentQueue == nil)
        {
            setupCurrentQueue();
        }
        
        lock.signal(); //Restituisce il Mutex
    }
    
    //Trova una coda a partire dal suo UUID
    func getCurrentQueue(_ UUID : String) -> MyQueue?
    {
        for queue in queueList
        {
            if(queue.beaconUUID == UUID)
            {
                return queue;
            }
        }
        return nil;
    }

    func ckQueueUpdate(_ flag: Bool){
        CloudKitHelper.modify(queue: currentQueue!) { (result) in
            switch result {
                case .success(let modifiedQueue):
                    for i in 0..<self.queueList.count {
                        if self.queueList[i].recordID == self.currentQueue?.recordID {
                            self.queueList[i] = modifiedQueue
                            self.queueList[i].joined = flag
                        }
                    }
                    print("Successfully modified")
                case .failure(let err):
                    print(err.localizedDescription)
            }
        }
    }
    
    func joinQueue()
    {
        listRefresh();
        if(currentQueue == nil)
        {
            //CodeFor: non c'è più nessuna coda collegata al tasto
            return;
        }
        else
        {
            currentQueue!.people.enqueue(user); //Si aggiunge alla coda
            //Istruzione che aggiorna queueList salvando dati sul server--

            lock.wait()
            ckQueueUpdate(true)
            lock.signal()
             //Notifica alla coda che ci si è uniti
        }
    }
        
    func leaveQueue()
    {
        //CodeFor: Istruzione che prende il Mutex del server
        listRefresh();
        if(currentQueue == nil)
        {
            //CodeFor: La lista non esiste più
            return
        }
        currentQueue!.remove(user);
        
        //CodeFor: Istruzione che aggiorna queueList salvando dati sul server--

        lock.wait()
        ckQueueUpdate(false)
        lock.signal()
        //CodeFor: Istruzione che restituisce il Mutex del server--
    }
       
    init(vc: ViewController) {
    
        beaconRegion = CLBeaconRegion(uuid: UUID(uuidString: Beacon.UUID)!, identifier: Beacon.identifier);
        beaconSet = Set<String>(); //Array dei beacons in zona
        locationManager = CLLocationManager();
        locationManager.delegate = vc;
         
        // UTILITY
        queueList = [MyQueue(people: Queue(), title: "Coda di test", avgWaitTime: 2.3, helpText: "Suggerimento", beaconUUID: Beacon.UUID, desc: "Description"), MyQueue(people: Queue(), title: "Coda di test 2", avgWaitTime: 2.4, helpText: "Suggerimento2", beaconUUID: "UUID2", desc: "Description2"), MyQueue(people: Queue(), title: "Coda di test 3", avgWaitTime: 2.5, helpText: "Suggerimento3", beaconUUID: "UUID3", desc: "Description3")] //SIMULO DI AVERE GIA' UNA LISTA; //Lista di queue disponibili
        currentQueueUUID = ""; //Lista attualmente collegata al bottone
        currentQueue = nil;
        locationManager.requestAlwaysAuthorization() //Richiede all'utente le autorizzazioni
        super.init()
        listRefresh(); //Aggiorno la lista
        startSearch(for: beaconRegion); //Comincio a cercare i Beacon
    }
}
