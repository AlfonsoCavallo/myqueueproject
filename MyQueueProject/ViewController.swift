//
//  ViewController.swift
//  MyQueueProject
//
//  Created by Alfonso Cavallo on 12/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController,CLLocationManagerDelegate{
    @IBOutlet var ExitButton: UIButton!
    let user = User();
    let databaseCheckTime =  3; //In secondi
    let currentQueuePropertiesCheckTime = 1 //In secondi
    var beacon: BeaconUtilities?
    var currIndex = 0 //L'indice corrente della coda, se essa è attiva
    var realPosition : Int
    {
        get
        {
            currIndex + 1;
        }
    }
    var currentQueue : MyQueue? //L'attuale coda
    {
        get
        {
            beacon!.currentQueue;
        }
    }
    var enqueued = false//Stabilisce se l'utente è in coda
    
//  Components Outlet
    @IBOutlet var TitleLabel: UILabel!
    @IBOutlet var DescLabel: UILabel!
    @IBOutlet var TimeLabel: UILabel!
    @IBOutlet var PeopleLabel: UILabel!
    
    
/*
   When the rounded button is pressed an alert appears asking about joining a queue, actions can be performed at the pressure of the yes - cancel button
   The button becomes green when pressed the first time (change to be performed here)
*/
    @IBOutlet var RoundedButton: RoundButton!
    
    override func viewDidLoad() {
//        super.viewDidLoad()
        beacon = BeaconUtilities.init(vc: self)
        listUpdate()
        Timer.scheduledTimer(timeInterval: TimeInterval(databaseCheckTime), target: self, selector: #selector(monitorQueue), userInfo: nil, repeats: true)
        monitorQueue()//Avvio del Thread che aggiorna le informazioni sulla coda attuale
        //centramento testo bottone
        RoundedButton.titleLabel?.textAlignment = NSTextAlignment.center
    }
    
    //Si esegue quando vengono trovati dei Beacon
    func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint) {
        var tempBeaconSet = Set<String>(); //Inizializza un set temporaneo
        for beacon in beacons
        {
          tempBeaconSet.insert(beacon.uuid.uuidString); //Inserisce nel set temporaneo tutti gli uuid dei Beacon trovati
        }
        //Se il Set temporaneo differisce dal Set allora il Set viene aggiornato
        if(!(tempBeaconSet == beacon!.beaconSet))
        {
            beacon!.beaconSet = tempBeaconSet;
          //Se la currentQueue non corrisponde ad un beacon trovato, la aggiorna
            if(!beacon!.beaconSet.contains(beacon!.currentQueueUUID))
            {
            beacon!.setupCurrentQueue();
            }
        }
        else
        {
            //CodeFor: Caso in cui i beacon rilevati sono sempre gli stessi
        }
    }
    
    @IBAction func RoundedButtonPressed(_ sender: UIButton) {
        
//        print(currentQueue?.beaconUUID)
    let alert = UIAlertController(title: "Queue alert", message: "Would you like to enqueue?", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
        (action: UIAlertAction!) in
        //Verifica che non ci si trovi già in una coda
        if(self.enqueued == false && self.beacon!.currentQueueUUID != "")
        {
            self.joinQueueOperations(); //Esegue le operazioni di unione alla coda
            self.RoundedButton.backgroundColor = UIColor(red:0.85, green:0.91, blue:1.00, alpha:1.0)
            //Per uscire dalla coda si deve premere exit
            self.ExitButton.isHidden = false
            self.RoundedButton.isUserInteractionEnabled = false
        }
        else if(self.beacon!.currentQueueUUID == "")
        {
            //Chiama la schermata di avviso che non ci sono code disponibili (AGGIUNGETE I COMMENTI ALLE ISTRUZIONI O QUANTO MENO A GRUPPI DI ISTRUZIONI)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let noQueueController = storyBoard.instantiateViewController(withIdentifier: "noQueueController") as! NoQueueController
            self.present(noQueueController, animated: true, completion: nil)
        }
        else
        {
            //Per uscire dalla coda si deve premere exit
        }
     }))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
     (action: UIAlertAction!) in }))
    present(alert, animated: true, completion: nil)
     
     
    }
    
    //Gestione uscita dalla coda(ALFONSO TE LA VEDI TU???)
    //Alfonso: "ROGER ROGER!"
    @IBAction func ExitPressed(_ sender: Any) {
//        MARK: ARE YOU SURE?
        let alertExit = UIAlertController(title: "Queue alert", message: "Do you really want to exit?", preferredStyle: .alert)
         alertExit.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
             (action: UIAlertAction!) in
            self.leaveQueueOperations();
            self.ExitButton.isHidden = true
            self.RoundedButton.isUserInteractionEnabled = true
            self.RoundedButton.titleLabel?.text = "Button"
         }))
        alertExit.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
         (action: UIAlertAction!) in }))
        present(alertExit, animated: true, completion: nil)
    }
    //Aggiorna i labels
    func labelUpdate()
    {
        DispatchQueue.main.async {
            //Aggiorna i labels
            if(self.currentQueue != nil)
            {
                //MARK: waiting time si resetta
                DispatchQueue.main.async {
                    if(!self.enqueued)
                    {
                        self.TimeLabel.text = String(self.currentQueue!.avgWaitTime * Float(self.currentQueue!.people.count)); //Moltiplica il tempo di attesa per l'indice attuale
                        self.PeopleLabel.text = String(self.currentQueue!.people.count); //Imposta il numero di persone in coda calcolando la lunghezza dell'array della MyQueue
                        self.RoundedButton.titleLabel!.text = String("Button");
                    }
                    else
                    {
//                        self.RoundedButton.titleLabel!.text = String(self.currentQueue!.userGetIndex(self.beacon!.user) + 1);
                        //Aggiorna l'indice corrente
                        self.TimeLabel.text = String(self.currentQueue!.avgWaitTime * Float(self.currIndex)); //Moltiplica il tempo di attesa per l'indice attuale
                        self.PeopleLabel.text = String(self.currentQueue!.people.count); //Imposta il numero di persone in coda calcolando la lunghezza dell'array della MyQueue
                    }
                }
            }
            else
            {
                //Vedi sopra
                DispatchQueue.main.async {
                    self.TimeLabel.text = "-"; //Tenere d'occhio questo possibile Warning
                    self.PeopleLabel.text = "-";
                }
            }
            DispatchQueue.main.async {
                self.TimeLabel.text!.append(" min"); //Aggiunge l'unità di misura di tempo
            }

        }
    }
    //ListRefresh completo del viewController
    func listUpdate()
    {
        //Attiva listRefresh
        beacon!.listRefresh();
        labelUpdate();
    }
    
    //Esegue tutte le istruzioni per uscire dalla coda
    func leaveQueueOperations()
    {
        self.beacon!.leaveQueue() //Lascia la coda sul server
        enqueued = false; //Avvisa che non si è più in coda
        RoundedButton.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    //Esegue tutte le istruzioni per accedere alla coda
    func joinQueueOperations()
    {
        self.beacon!.joinQueue(); //Si unisce alla coda tramite il server
        enqueued = true; //Notifica tramite un flag che si è in coda
        self.RoundedButton.titleLabel?.text = String(currentQueue?.people.count ?? 32)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        switch segue.identifier {
//          Passing queueList to ListView
            case "ShowList"?:
                let dest = segue.destination as! ListView
                dest.arrayQueue = beacon?.queueList ?? [MyQueue]()
                dest.queueView = self
                listUpdate();
            default: print(#function)
            }
            // Pass the selected object to the new view controller.
        }
    //Codice del Thread che aggiorna i parametri a schermo
    
    //Codice del Thread che monitora la coda
    @objc func monitorQueue()
    {
        DispatchQueue.global(qos: .default).async {
                //Attende il tempo di aggiornamento
                //Se non si è più in coda smette di monitorare
//                print("\(self.enqueued)")
                self.listUpdate();
                if(self.enqueued == false)
                {
                    return;
                }
                 //Aggiorna la lista
                //Se la lista corrente non è più sul database esegue questo codice
                if(self.currentQueue == nil)
                {
                     //CodeFor: La lista non esiste più
                    self.enqueued = false; //Notifica tramite un flag che non si è più in coda
                    //Disabilita il Joined a tutte le code
                    for elem in self.beacon!.queueList
                    {
                        elem.joined = false;
                    }
                    return;
                }
                self.currIndex = self.currentQueue!.userGetIndex(self.beacon!.user); //Ottiene l'indice corrente
                //Gestisce le varie possibilità per avvisarti dell'attuale situazione della coda
                if(self.currIndex == -1)
                {
                    //CodeFor: Gestisce la possibilità che l'utente non sia più in coda
                    self.enqueued = false; //Notifica tramite un flag che non si è più in coda
                    return;
                }
                else if(self.realPosition == 2)
                {
                     //CodeFor: Ti dice che sei il prossimo
                }
                else if(self.realPosition == 1)
                {
                     //CodeFor: E' il tuo turno
                }
        }
    }
}



