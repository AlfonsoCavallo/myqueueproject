//
//  NoQueueController.swift
//  MyQueueProject
//
//  Created by Angelo Vistocco on 16/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class NoQueueController: UIViewController {

    @IBOutlet var ButtonDismiss: UIButton!
    @IBOutlet var PopupView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        PopupView.layer.cornerRadius = 10
        PopupView.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func DismissPopup(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
