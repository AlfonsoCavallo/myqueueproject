//
//  Beacon.swift
//  MyQueueProject
//
//  Created by Alfonso Cavallo on 13/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

struct Beacon
{
    static var UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"
    static var majorValue = 1;
    static var minorValue = 12;
    static var identifier = "8f34c9e8c84629201f5a080a82788406";
}
