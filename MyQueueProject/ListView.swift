//
//  ListView.swift
//  MyQueueProject
//
//  Created by Angelo Vistocco on 13/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class ListView: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    var arrayQueue: [MyQueue]?
    var queueView: ViewController?
    var filteredQueues: [MyQueue]?

    
    @IBOutlet var searchBAr: UISearchBar!
    @IBOutlet var Table: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        Table.keyboardDismissMode = .onDrag
        Table.delegate = self
        Table.dataSource = self
        searchBAr.delegate = self

    }

//    TABLE VIEW CONTROLLING

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(searchActive) {
                return filteredQueues?.count ?? 0
            }
            return arrayQueue?.count ?? 0
        }
   
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! QueueCell
            if(searchActive) {
                cell.queueTitle.text = filteredQueues?[indexPath.row].title ?? ""
                cell.queueTime.text = String((filteredQueues?[indexPath.row].avgWaitTime)! * Float((filteredQueues?[indexPath.row].people.count)!) ?? 0)
                cell.queuePeople.text = String(filteredQueues?[indexPath.row].people.count ?? 0)
                if filteredQueues?[indexPath.row].joinable == false {
                    cell.queueStatus.tintColor = UIColor(red:0.38, green:0.42, blue:0.50, alpha:1.0)
                }
                else if filteredQueues?[indexPath.row].joinable == true &&  filteredQueues?[indexPath.row].joined == false {
                    cell.queueStatus.tintColor = UIColor(red:0.72, green:0.80, blue:0.52, alpha:1.0)
                }
                else {
                    cell.queueStatus.tintColor = UIColor(red:1.00, green:0.79, blue:0.50, alpha:1.0)                }
            }
            else{
                cell.queueTitle.text = arrayQueue?[indexPath.row].title ?? ""
                cell.queueTime.text = String((arrayQueue?[indexPath.row].avgWaitTime)! * Float((arrayQueue?[indexPath.row].people.count)!) ?? 0)
                cell.queuePeople.text = String(arrayQueue?[indexPath.row].people.count ?? 0)
                
                if arrayQueue?[indexPath.row].joinable == false {
                    cell.queueStatus.tintColor = UIColor(red:0.38, green:0.42, blue:0.50, alpha:1.0)
                }
                else if arrayQueue?[indexPath.row].joinable == true &&  arrayQueue?[indexPath.row].joined == false {
                    cell.queueStatus.tintColor = UIColor(red:0.72, green:0.80, blue:0.52, alpha:1.0)
                }
                else {
                    cell.queueStatus.tintColor = UIColor(red:1.00, green:0.79, blue:0.50, alpha:1.0)
//                    MARK: restituire posizione indice
                }
            }
            return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        cell selected
        let selectedQueue = arrayQueue?[indexPath.row]
        
        //      Find current user id in the selected queue
        let index = selectedQueue!.userGetIndex(queueView!.beacon!.user)
        
        //queueView?.beacon?.currentQueueUUID = selectedQueue?.beaconUUID ?? "" QUESTO RIMANE QUI COME MONITO, PER RICORDARE CHE L'INCAPSULAMENTO E' UNA COSA SACROSANTA
        if((selectedQueue != nil && !queueView!.enqueued && selectedQueue!.joinable) || (queueView!.enqueued && selectedQueue != nil && selectedQueue!.joined)) //Verifica che la coda selezionata non sia vuota e se è valida ne effetua il Setup
        {
            if(index == -1) {
            //        MARK: BUTTON DA CAMBIARE
                    queueView?.RoundedButton.titleLabel?.text = "Button"
                    queueView?.RoundedButton.backgroundColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
                    queueView?.ExitButton.isHidden = true
                    queueView?.RoundedButton.isUserInteractionEnabled = true
                }
                else {
                    queueView?.RoundedButton.titleLabel?.text = String(index)
                    queueView?.RoundedButton.backgroundColor = UIColor(red:0.85, green:0.91, blue:1.00, alpha:1.0)
                    queueView?.ExitButton.isHidden = false
                    queueView?.RoundedButton.isUserInteractionEnabled = false
                }
            if(queueView!.beacon!.beaconSet.contains(selectedQueue!.beaconUUID))
           {
                //Aggiorna tutte le possibili informazioni
                queueView?.TitleLabel.text = selectedQueue?.title
                queueView?.DescLabel.text = selectedQueue?.desc
                queueView?.beacon!.setupCurrentQueue(selectedQueue!)
//                queueView!.labelUpdate();
            }
            self.dismiss(animated: true)
        }
        print(queueView?.currentQueue?.beaconUUID ?? "ERRORE NELLA LISTVIEW")
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
     
    //Metodo che aggiorna la lista premendo il tasto aggiorna
    @IBAction func RefreshList(_ sender: Any) {
        queueView!.beacon?.listRefresh(); //Aggiorna la lista
        Table.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
 
//    MARK: search bar
    var searchActive: Bool = false
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredQueues = arrayQueue?.filter({ (queue) -> Bool in
            let queueTitle = queue.title
            let temp: NSString = queueTitle as NSString
            let range = temp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(filteredQueues?.count == 0) {
            searchActive = false
        } else {
            searchActive = true
        }
        self.Table.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

