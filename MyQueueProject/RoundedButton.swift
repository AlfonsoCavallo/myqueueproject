//
//  RoundedButton.swift
//  MyQueueProject
//
//  Created by Alessandro D'Apice on 13/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//


// This class implements a rounded button
 
import UIKit

@IBDesignable
class RoundButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
        self.layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
