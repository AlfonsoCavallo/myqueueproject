//
//  User.swift
//  MyQueueProject
//
//  Created by Tommaso Di Maio on 13/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import Foundation
// Codable serve per eseguire la serializzazione json
struct User: Codable, Equatable{
    var uuid:String
    init(){
        self.uuid = UUID().uuidString;
    }
    init(uuid: String) {
        self.uuid = uuid
    }
    //Confronta due utenti
    static func == (u1: User, u2: User) -> Bool {
        return u1.uuid == u2.uuid;
    }
}
