//
//  MyQueue.swift
//  MyQueueProject
//
//  Created by Tommaso Di Maio on 13/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import Foundation
import CloudKit

class MyQueue {
    var people:Queue<User>
    var title:String
    var desc:String?
    var avgWaitTime:Float
    var helpText:String?
    var beaconUUID:String
    var recordID: CKRecord.ID?
    var joinable:Bool=true
    var joined:Bool=false
    
    
    init(people:Queue<User>,title:String,avgWaitTime:Float,helpText:String,beaconUUID:String) {
        self.people = people
        self.title = title
        self.avgWaitTime = avgWaitTime
        self.helpText = helpText
        self.beaconUUID = beaconUUID
        self.desc = nil
    }
    init(people:Queue<User>,title:String,avgWaitTime:Float,beaconUUID:String,
         desc: String, recordID: CKRecord.ID) {
        self.people=people
        self.title=title
        self.avgWaitTime=avgWaitTime
        self.beaconUUID=beaconUUID
        self.desc=desc
        self.recordID = recordID
    }
    init(people:Queue<User>,title:String,avgWaitTime:Float, helpText:String, beaconUUID:String,
         desc: String) {
        self.people=people
        self.title=title
        self.avgWaitTime=avgWaitTime
        self.helpText = helpText
        self.beaconUUID=beaconUUID
        self.desc=desc
    }
    
    //Restituisce l'indice dello User oppure -1 se non lo trova
    func userGetIndex(_ user : User) -> Int
    {
        var i = 0;
        //Itera sull'array e restituisce l'indice dell'elemento se lo trova, altrimenti -1
        for person in people.array
        {
            if(person == user)
            {
                return i;
            }
            i += 1;
        }
        return -1;
    }
    
    //Rimuove un elemento dalla coda
    func remove(_ user : User) -> User?
    {
        //Filtra l'array rimuovendo l'elemento da escludere
        let filteredArray = people.array.filter() {$0 != user}
        //Se l'array filtrato corrisponde in dimensioni a quello precedente allora non è stato rimosso nulla
        if(filteredArray.count != people.array.count)
        {
            people.array = filteredArray;
            return user;
        }
        return nil;
    }
}
