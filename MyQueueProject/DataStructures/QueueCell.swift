//
//  QueueCell.swift
//  MyQueueProject
//
//  Created by Angelo Vistocco on 14/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class QueueCell: UITableViewCell {

    @IBOutlet var queueTitle: UILabel!
    @IBOutlet var queueTime: UILabel!
    @IBOutlet var queuePeople: UILabel!
    @IBOutlet var queueStatus: UIImageView!
    
    override func awakeFromNib() {
       super.awakeFromNib()
        // Initialization code
    }

   override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(title: String, number: String) {
        queueTitle.text = title
        queueNumber.text = number
    }
}
