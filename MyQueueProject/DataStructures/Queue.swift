//
//  Queue.swift
//  MyQueueProject
//
//  Created by Tommaso Di Maio on 13/02/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import Foundation

// Codable serve per eseguire la serializzazione json
public struct Queue<T: Codable>: Codable{
   var array = [T]()

  public var count: Int {
    return array.count
  }

  public var isEmpty: Bool {
    return array.isEmpty
  }

  public mutating func enqueue(_ element: T) {
    array.append(element)
  }

  public mutating func dequeue() -> T? {
    if isEmpty {
      return nil
    } else {
      return array.removeFirst()
    }
  }

  public var front: T? {
    return array.first
  }
    init(array:[T]){
        self.array=array
    }
    init(){
        
    }
    
}


